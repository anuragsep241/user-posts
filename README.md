# UserPosts

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

## Setup
Run `npm install` in project folder terminal
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests. 100% coverge.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests. Please update chrome browser to latest if e2e test are  giving error.
Covering all the scenarios in end to end test cases.


## Project structure

Post is considered as a stand alone component which is a dumb ui component `app-conversation-details`.
To display in tabular formate, a saperate component `app-conversation-list` is created.
Both above components are part of feature-module `conversation-module`.
To get the resposnse from Backend and manage the click state, Service `ConversationService` is created.
Code for managing click state using RxJs, commented and having comment abobe it inside file.