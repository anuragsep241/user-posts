import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  async getPostHeaderText() {
    return await element(by.name('post-header')).getText();
  }

  async getPostHeader() {
    return await element(by.name('post-header'));
  }
}
