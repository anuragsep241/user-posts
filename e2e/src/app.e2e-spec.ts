import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display Post Id as header for a post-card', () => {
    page.navigateTo();
    expect(page.getPostHeaderText()).toEqual('Post Id: 1');
  });

  it('should display User Id as header for a post-card after click', async () => {
    page.navigateTo();
    const postCard = await page.getPostHeader();
    postCard.click();
    expect(page.getPostHeaderText()).toEqual('User Id: 1');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
