import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FeaturesModule } from './feature-modules/features.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FeaturesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
