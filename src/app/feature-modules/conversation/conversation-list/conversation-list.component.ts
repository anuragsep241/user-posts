import { Component, OnInit } from '@angular/core';
import { ConversationService } from '../conversation.service';
import { Conversation} from '../conversation.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.scss']
})
export class ConversationListComponent implements OnInit {
  conversationList$: Observable<Array<Conversation>>;
  constructor(private conversationService: ConversationService) {}

  ngOnInit() {
    this.conversationList$ = this.conversationService.getConversations();
  }
}
