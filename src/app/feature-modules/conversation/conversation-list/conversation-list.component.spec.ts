import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, async, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs/internal/observable/of';
import { Conversation } from '../conversation.model';
import { ConversationModule } from '../conversation.module';
import { ConversationService } from '../conversation.service';
import { ConversationListComponent } from './conversation-list.component';

const mockConversations: Conversation[] = [
  {id: 1, userId: 1, title: 'test title 1', body: 'test body 1'},
  {id: 2, userId: 2, title: 'test title 2', body: 'test body 2'}
];

class MockConversationSerivce {
  getConversations() {
    return of(mockConversations);
  }
}

describe('ConversationListComponent', () => {
  let component;
  let fixture;
  beforeEach(async(() => {
    let injector;
    let service: ConversationService;

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, ConversationModule],
      declarations: [  ],
      providers: [
        { provide: ConversationService, useClass: MockConversationSerivce }
      ]
    })
    .compileComponents();

    injector = getTestBed();
    service = injector.get(ConversationService);
    fixture = TestBed.createComponent(ConversationListComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should call the service api', () => {
    component.ngOnInit();
    component.conversationList$.subscribe((data) => {
      expect(data.length).toBe(2);
    });
  });
});
