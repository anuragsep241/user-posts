import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { Conversation } from './conversation.model';
import { ConversationService } from './conversation.service';


describe('ConversationService', () => {
  let injector;
  let service: ConversationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ConversationService]
    });

    injector = getTestBed();
    service = injector.get(ConversationService);
    httpMock = injector.get(HttpTestingController);
  });

  describe('#getConversations', () => {
    it('should return an Observable<Conversation[]>', () => {
      const mockConversations: Conversation[] = [
        {id: 1, userId: 1, title: 'test title 1', body: 'test body 1'},
        {id: 2, userId: 2, title: 'test title 2', body: 'test body 2'}
      ];

      service.getConversations().subscribe(conversations => {
        expect(conversations.length).toBe(2);
        expect(conversations).toEqual(mockConversations);
      });

      const req = httpMock.expectOne(`https://jsonplaceholder.typicode.com/posts`);
      expect(req.request.method).toBe('GET');
      req.flush(mockConversations);
    });
  });
});
