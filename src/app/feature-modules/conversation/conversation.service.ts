import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Conversation } from './conversation.model';

@Injectable({
    providedIn: 'root',
  })
export class ConversationService {

    // Shared state when no rxjs is used
    isIdVisible = true;

    // Shared state when RXJS is used
    isIdVisible$ = new BehaviorSubject(true);

    constructor(private http: HttpClient) {
    }

    getConversations(): Observable<Array<Conversation>> {
        return this.http.get<any[]>(`https://jsonplaceholder.typicode.com/posts`);
    }
}
