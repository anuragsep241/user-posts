export interface Conversation {
    id: number;
    userId: number;
    title: string;
    body: string;
}
