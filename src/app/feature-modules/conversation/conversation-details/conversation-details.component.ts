import { Component, Input } from '@angular/core';
import { Conversation } from '../conversation.model';
import { ConversationService } from '../conversation.service';

@Component({
  selector: 'app-conversation-detail',
  templateUrl: './conversation-details.component.html',
  styleUrls: ['./conversation-details.component.scss']
})
export class ConversationDetailsComponent {
  title = 'user-posts';
  @Input() conversation: Conversation;
  constructor(private conversationService: ConversationService) {}

  get isIdVisible() {
    // when shared stae is not using RxJs
    return this.conversationService.isIdVisible;

    // when shared state is using RxJs
    // return this.conversationService.isIdVisible$.value;
  }

  /**
   * Methode to toggle the state for all the posts
   */
  switchId() {
    // when shared stae is not using RxJs
    this.conversationService.isIdVisible = !this.conversationService.isIdVisible;
    // when shared state is using RxJs
    // this.conversationService.isIdVisible$.next(!this.conversationService.isIdVisible$.value);
  }
}
