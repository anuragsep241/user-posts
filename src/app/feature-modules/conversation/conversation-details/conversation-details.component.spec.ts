import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, async, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs/internal/observable/of';
import { Conversation } from '../conversation.model';
import { ConversationService } from '../conversation.service';
import { ConversationDetailsComponent } from './conversation-details.component';

class MockConversationSerivce {
  isIdVisible = true;
}

describe('ConversationDetailsComponent', () => {
  let fixture;
  let component;
  beforeEach(async(() => {
    let injector;
    let service: ConversationService;

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ ConversationDetailsComponent ],
      providers: [
        { provide: ConversationService, useClass: MockConversationSerivce }
      ]
    })
    .compileComponents();

    injector = getTestBed();
    service = injector.get(ConversationService);
    fixture = TestBed.createComponent(ConversationDetailsComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should create the component and userId is visible', () => {
    expect(component).toBeTruthy();
    expect(component.isIdVisible).toBeTruthy();
  });

  it('should call the switchId function', () => {
    component.switchId();
    expect(component.isIdVisible).toBeFalsy();
  });
});
