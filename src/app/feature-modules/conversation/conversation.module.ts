import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ConversationListComponent } from './conversation-list/conversation-list.component';
import { ConversationDetailsComponent } from './conversation-details/conversation-details.component';

@NgModule({
  declarations: [
    ConversationListComponent,
    ConversationDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  exports: [
    ConversationListComponent,
    ConversationDetailsComponent
  ]
})
export class ConversationModule { }
