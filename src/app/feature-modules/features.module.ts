import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ConversationModule } from './conversation/conversation.module';

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    ConversationModule
  ],
  providers: [],
  exports: [ConversationModule]
})
export class FeaturesModule { }
